a2: Apple II Diskette Images
============================

All images below are 5.25" 140 KiB 16-sector diskettes, unless
otherwise specified.

* `EMPTY-DOS33-48K-V254.dsk`: Bootable DOS 3.3 48K slave diskette with
  only a one-line Applesoft `HELLO`. Formatted on an Apple IIc with
  v255 ROM.
* `EMPTY-DOS33-48K-V170.dsk`: Differs from `-V254` only in the volume
  number, which is $AA (170 decimal). Boots fine on an Apple IIc with
  v255 ROM.


Emulator Problems
-----------------

Both diskette images work fine when sent to an Apple IIc and written
to a floppy using ADTPro 2.0.3. Doing a [raw track dump][dump]
confirms that the sector address headers contain the correct volume
number.

`EMPTY-DOS33-48K-V254.dsk` seems to work fine in the [linapple] and
[Apple IIjs] emulators, but the `-V170` version has problems in both.
Booting a V254 from drive 1, mounting V170 in drive 2 and typing
`CATALOG,D2` shows an incorrect volume number of 254 in both
emulators.

Trying to boot the V170 diskette in linapple drops into the monitor:

    9D89-    A=FB X=38 Y=1B P=B5 S=FB       # II, II+, IIe
    9D88-    A=FD X=38 Y=1B P=37 S=FB       # Enhanced IIe

AppleIIjs drops to the monitor prompt (with no register display) after
V170 is attached to drive 1. Power-cycling removes the diskette, but
forcing a cold boot by invaliding `PWREDUP` (`3F4:0` followed by a
reset) produces the same behaviour. Using (non-Autostart) II emulation
and booting with `6^P` gives:

    9D88-    A=FD X=38 Y=1B P=B0 S=FB



<!-------------------------------------------------------------------->
[Apple IIjs]: https://www.scullinsteel.com/apple2/
[dump]: https://gitlab.com/retroabandon/apple2/-/tree/master/disk
[linapple]: https://github.com/linappleii/linapple
