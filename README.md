osimg - BIOS ROM and DOS Disk Images for Test Frameworks
========================================================

This repository contains BIOS ROM and DOS disk images useful for testing in
emulators and on real machines, both with automated test frameworks and
manual tests.

Contents:

- `README.md`: This file.

- `a2/`: Apple II DOS images.
  - `EMPTY-DOS33-48K-V170.dsk`: DOS 3.3; volume number 170; no files.
  - `EMPTY-DOS33-48K-V254.dsk`: DOS 3.3; volume number 254; no files.

- `mb6885/`: Hitachi MB-6885 Basic Master Jr. ROM images. These are named
  appropriately for the [bm2] emulator to load with the `-rom_dir=…/mb6885`
  configuration option.
  - `bas.rom`: $B000-$DFFF: Hitachi BASIC ROM
  - `prt.rom`: $E000-$E7FF: "Printer" ROM
  - `mon.rom`: $F000-$FFFF: Monitor/BIOS ROM
